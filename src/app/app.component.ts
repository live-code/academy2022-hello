import { Component } from '@angular/core';
import { User } from './model/user';

@Component({
  selector: 'app-root',
  template: `
    <div class="container mt-5">
      <li 
        class="list-group-item"
        *ngFor="let user of users"
        (click)="selectUser(user)"
      >
        {{user.name}} ( {{user.gender}} ) 
        
        <i class="fa fa-trash" (click)="deleteUser(user)"></i>
      </li>
      
      <div class="my-modal card" *ngIf="selectedUser">
        <div class="card card-header">
          {{selectedUser.name}}
        </div>
        <div class="card-body">
          <img
            width="100%"
            [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + selectedUser?.city + '&size=1200,400'" alt="">
        </div>
        <button (click)="selectedUser = null"> close modal</button>
      </div>
    </div>
  `,
  styles: [`
    .my-modal {
      position: fixed;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
    }
  `]
})
export class AppComponent {
  users: User[] = [
    {
      id: 1,
      name: 'Fabio',
      age: 45,
      gender: 'F',
      city: 'Palermo',
      birthday: 1598910648000,
      bitcoins: 1.223423532532,
    },
    {
      id: 2,
      name: 'Maria',
      age: 15,
      gender: 'F',
      city: 'Trento',
      birthday: 1198910648000,
      bitcoins: 1.345532532523523,
    },
    {
      id: 3,
      name: 'Ciccio',
      age: 25,
      gender: 'M',
      city: 'Milano',
      birthday: 1198910648000,
      bitcoins: 1.345532532523523,
    }
  ];
  selectedUser: User | null = null;

  constructor() {
    setTimeout(() => {
      this.users.push({
        id: 4,
        name: 'Pippo',
        age: 15,
        gender: 'F',
        city: 'Trento',
        birthday: 1198910648000,
        bitcoins: 1.345532532523523,
      })
    }, 2000)
  }

  selectUser(u: User) {
    this.selectedUser = u;
  }

  deleteUser(u: User) {
    // array ES6 methods
    const index = this.users.findIndex(user => user.id === u.id)
    if (index !== -1) {
      this.users.splice(index, 1);
      this.selectedUser = null;
    }

  }

}

